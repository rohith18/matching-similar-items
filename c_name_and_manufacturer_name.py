import json
import glob
from openpyexcel import load_workbook

files = glob.glob("./evital_slugs/evital_slugs*.json")
dict_file = {}
count = 0
item = []
manufacturer_name_list = []
for file in files:
    with open(file) as file:
        dict_file = { **dict_file, **(json.load(file)) }

for key in dict_file:
    if ((dict_file[key] != None and dict_file[key]["data"] != None and dict_file[key]["data"]["manufacturer_name"] != None) and (dict_file[key] != "" and dict_file[key]["data"] != "" and dict_file[key]["data"]["manufacturer_name"] != "")):
        # print(dict_file[key]["data"]["manufacturer_name"])
        manufacturer_name_list.append(dict_file[key]["data"]["manufacturer_name"])
        count = count + 1

print("No. of Items in manufacturer_name_list: ", len(manufacturer_name_list))
manufacturer_name_set = set(manufacturer_name_list)
print("Length of manufacturer_name_set: ", len(manufacturer_name_set))

book = load_workbook("Book1.xlsx")
sheet = book["Sheet1"]
count = 0
c_name_list = []
item1 = []

rows = sheet.rows

headers = [ cell.value for cell in next(rows) ]

all_rows= []

for row in rows:
    data = {}
    for title, cell in zip(headers, row):
        data[title] = cell.value
    all_rows.append(data)

for item in all_rows:
    # print(item.get("c_name"))
    c_name_list.append(item.get("c_name"))
    count = count + 1

c_name_set = set(c_name_list)
print("c_name_list: ",len(c_name_list))
print("c_name_set: ",len(c_name_set))










