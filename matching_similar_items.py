import json
import glob
import csv
from xlsxwriter import Workbook
from fuzzywuzzy import fuzz
from evital_manufacturer_name import c_name_set, manufacturer_name_set

workbook = Workbook('similar_data_3.xlsx')
w_sheet = workbook.add_worksheet()

w_sheet.write(0, 0, 'c_name')
w_sheet.write(0, 1, 'manufacturer_name')

similar_c_name_list = []
similar_manufacturer_name_list = []

for company in c_name_set:
    for manufacturer in manufacturer_name_set:
        if(fuzz.token_sort_ratio(company,manufacturer) > 95):
            similar_c_name_list.append(company)
            similar_manufacturer_name_list.append(manufacturer)
            
not_similar_c_name_list = [i for i in c_name_set if i not in similar_c_name_list]
not_similar_manufacturer_name_list = [i for i in manufacturer_name_set if i not in similar_manufacturer_name_list]

for i in not_similar_c_name_list:
    if i not in not_similar_manufacturer_name_list:
        similar_c_name_list.append(i)
        similar_manufacturer_name_list.append("")
for i in not_similar_manufacturer_name_list:
    if i not in not_similar_c_name_list:
        similar_c_name_list.append("")
        similar_manufacturer_name_list.append(i)

# Write the column data.
w_sheet.write_column(1, 0, similar_c_name_list)
w_sheet.write_column(1, 1, similar_manufacturer_name_list)

workbook.close()